(function ($) {
	$.fn.puissance4 = function(options) {
    	var defaults = {
    		'x': 7,
    		'y': 6,
    		'nameJoueur1': 'joueur1',
    		'nameJoueur2': 'joueur2',
    		'joueur1': 'rgb(255, 0, 0)',
    		'joueur2': 'rgb(255, 255, 0)'
    	};
    	$.extend(defaults, options);
    	var columns;
    	var canPlay = true;
    	$('body').append('<div id="dernierScore"><h3>Dernière partie:</div>');
    	$('#dernierScore').css({
    		'display': 'inline-block',
    		'vertical-align': 'top',
    		'margin-left': '2%',
    		'border': '1px solid black',
    		'border-radius': '20px'
    	});
    	$(this).css({
    		'background-color': 'blue',
    		'display': 'inline-block',
    		'width': defaults.x * 2 + defaults.x * 55 + 'px',
    		'vertical-align': 'top',
    		'position' : 'relative'
    	});
		for (var i = 1; i <= defaults.x; i++) {
			$(this).append('<div id="columns' + i + '" data-color="none"></div>');
			$('#columns' + i).css({
				'display': 'inline-block',
				'margin': '2px'
			});
			if (i === 1) {
				columns = '#columns' + i;
			} else {
				columns += ', #columns' + i;
			}
			for (var j = 1; j <= defaults.y; j++) {
				$('#columns' + i).append('<div class="case' + j + '" data-color="none"></div>');
				$('.case' + j).css({
					'background-color': 'white',
					'width': '50px',
					'min-height': '50px',
					'border-radius': '50%',
					'border': '1px solid black',
					'margin-top': '2px'
				});
			}
		}
		$(this).append('<button id="back">Annuler</button>');
		$(this).append('<div id="tourJoueur">' + defaults.nameJoueur1 + ', à vous de jouer !');
		$('#tourJoueur').css({
			'color': defaults.joueur1,
			'float': 'right'
		});
		var colorPlayer1 = defaults.joueur1, colorPlayer2 = defaults.joueur2, click = 0, backCase, back, selecteur = $(this).selector;
		$(this).click(function() {
			click += 1;
		});
		function memory(winner) {
			for (var i = 1; i <= 10; i++) {
				if (!localStorage.getItem('Partie' + i)) {
					localStorage.setItem('Partie' + i, winner + ' à gagné la partie ' + i);
					break;
				} else if (i === 10) {
					for (var j = 1; j <= 10; j++) {
						localStorage.removeItem('Partie' + j);
					}
					localStorage.setItem('Partie1', winner + ' à gagné la partie 1');
				}
			}
		}
		function win() {
			if (click%2 === 0) {
				alert(defaults.nameJoueur1 + ' a gagner !');
				memory(defaults.nameJoueur1);
				location.reload();
			} else {
				alert(defaults.nameJoueur2 + ' a gagner !');
				memory(defaults.nameJoueur2);
				location.reload();
			}
		}

		function createJeton(x,y,color) {
			var top = -60;
			var NewTop = $('.case'+y).position().top + 3;
			var left = $('#columns'+x).position().left + 3;
			$(selecteur).append('<div style="width:50px;height:50px;border-radius:50%;background-color:'+color+';position:absolute;top:'+top+'px;left:'+left+'px;" class="jetons jeton-'+x+'-'+y+'"></div>');
			$('.jeton-'+x+'-'+y).animate({'top':NewTop+'px'},function(){
				canPlay = true;
				checkWinVerti('#columns'+x, color);
				checkWinHori(y, color);
				checkWinDiag('.case' + y, 'columns'+x, color);
				changePlayer();
			});
		}

		function checkWinVerti(columns, win_color) {
			var color, nbr = 0;
			for (var i = 1; i <= defaults.y; i++) {
				color = $(columns + ' .case' + i).attr('data-color');
				if (color === win_color) {
					nbr ++;
					if (nbr === 4) {
						win();
					}
				} else {
					nbr = 0;
				}
			}
		}
		function checkWinHori(cas, win_color) {
			var color, nbr = 0;
			for (var i = 1; i <= defaults.x; i++) {
				color = $('#columns' + i + ' .case' + cas).attr('data-color');
				if (color === win_color) {
					nbr ++;
					if (nbr === 4) {
						win();
					}
				} else {
					nbr = 0;
				}
			}
		}
		function checkWinDiag(cas, columns, win_color) {
			var color, nbr = 0, columnsbegin, casBegin, columnsBis, casBis;
			columnsBis = columns.match(/\d+/);
			casBis = cas.match(/\d+/);
			for (var i = columnsBis, j = casBis; i <= defaults.x; i++, j--) {
				if (j === 1) {
					columnsbegin = i;
					casBegin = j;
					break;
				}
				columnsbegin = i;
				casBegin = j;
			}
			for (i = columnsbegin, j = casBegin; i >= 1; i--, j++) {
				color = $('#columns' + i + ' .case' + j).attr('data-color');
				if (color === win_color) {
					nbr++;
					if (nbr === 4) {
						win();
					}
				} else {
					nbr = 0;
				}
			}
			for (i = columnsBis, j = casBis; i >= 1; i--, j--) {
				if (j === 1) {
					columnsbegin = i;
					casBegin = j;
					break;
				}
				columnsbegin = i;
				casBegin = j;
			}
			for (i = columnsbegin, j = casBegin; i <= defaults.x; i++, j++) {
				color = $('#columns' + i + ' .case' + j).attr('data-color');
				if (color === win_color) {
					nbr++;
					if (nbr === 4) {
						win();					}
				} else {
					nbr = 0;
				}
			}
		}

		function checkNull() {
			if($('.jetons').length == defaults.x * defaults.y){
				alert('partie null');
				location.reload();
			}
		}

		function changePlayer() {
			if (click%2 != 0) {
				$('#tourJoueur').html(defaults.nameJoueur2 + ', à votre tour !');
				$('#tourJoueur').css('color', colorPlayer2);
			} else {
				$('#tourJoueur').html(defaults.nameJoueur1 + ', à vous de jouer !');
				$('#tourJoueur').css('color', colorPlayer1);
			}
		}
		function checkJeton(element) {
			var data;
			for (var i = 1, j = 0; i <= defaults.y; i++, j++) {
				data = $(element + ' .case' + i).data('color');
				if ($(element + ' .case' + i).attr('data-color') === colorPlayer2 || $(element + ' .case' + i).attr('data-color') === colorPlayer1) {
					if ($('.jetons.jeton-' + element.substr(-1) + '-' + j).css('background-color') === colorPlayer2 || $('.jetons.jeton-' + element.substr(-1) + '-' + j).css('background-color') === colorPlayer1) {
						break;
					} else {
						if (click%2 === 0) {
							$(element + ' .case' + j).attr('data-color', colorPlayer1);
							createJeton(element.substr(-1),j,colorPlayer1);
							back = element + ' .case' + j;
							backCase = '.jetons.jeton-' + element.substr(-1) + '-' + j;
							if (j === 0) {
								click --;
							}
							changePlayer();
							break;
						} else {
							createJeton(element.substr(-1),j,colorPlayer2);
							$(element + ' .case' + j).attr('data-color', colorPlayer2);
							back = element + ' .case' + j;
							backCase = '.jetons.jeton-' + element.substr(-1) + '-' + j;
							if (j === 0) {
								click --;
							}
							changePlayer();
							break;
						}
					}
				} else {
					if ($(element + ' .case' + defaults.y).attr('data-color') != colorPlayer1 && $(element + ' .case' + defaults.y).attr('data-color')	 != colorPlayer2) {
						if ($('.jetons.jeton-' + element.substr(-1) + '-' + defaults.y).css('background-color') === colorPlayer2 || $('.jetons.jeton-' + element.substr(-1) + '-' + defaults.y).css('background-color') === colorPlayer1) {
							break;
						} else {
							if (click%2 === 0) {
								createJeton(element.substr(-1),defaults.y,colorPlayer1);
								$(element + ' .case' + defaults.y).attr('data-color', colorPlayer1);
								back = element + ' .case' + defaults.y;
								backCase = '.jetons.jeton-' + element.substr(-1) + '-' + defaults.y;
								break;
							} else {
								createJeton(element.substr(-1),defaults.y,colorPlayer2);
								$(element + ' .case' + defaults.y).attr('data-color', colorPlayer2);
								back = element + ' .case' + defaults.y;
								backCase = '.jetons.jeton-' + element.substr(-1) + '-' + defaults.y;
								break;
							}
						}
					}
				}
			}
		}
		$(columns).click(function() {
			if(!canPlay){
				return false;
			}
			canPlay = false;
			var id = event.currentTarget.id;
			checkJeton('#' + id);
		});
		$('#back').click(function() {
			console.log(backCase);
			$(backCase).fadeOut(300, function(){ $(this).remove();});
			$(back).attr('data-color', 'none');
		});
		for (var s = 1; s <= 10; s++) {
			if (localStorage.getItem('Partie' + s)) {
				$('#dernierScore').append('<p><b>Gagnant de la partie ' + s + ': </b>' + localStorage.getItem('Partie' + s) + '</p>');
			} else {
				break;
			}
		}
	};
}(jQuery));